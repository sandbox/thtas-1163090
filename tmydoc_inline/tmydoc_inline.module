<?php


function tmydoc_inline_theme($existing, $type, $theme, $path) {
  return array(
    'tmydoc_inline_doc_hints' => array(
      'arguments' => array('node' => NULL),
    ),
    'tmydoc_inline_folder_hints' => array(
      'arguments' => array('node' => NULL),
    ),
  );
}

function theme_tmydoc_inline_doc_hints($node)
{
	$header = array(t("Code"),t("Result"));
	$rows = array(
		array("[doc:".$node->nid."]",theme('tmydoc_nodefiles',$node)),
		array("[docicon:".$node->nid."]",theme('tmydoc_nodefiles',$node,$title=FALSE)),
	);
	
	//$caption="Inline document link code";
	$output = theme("table",$header,$rows,$atts=NULL,$caption);
	$output .= '<div>'.t('Use the codes in this table to insert this document in to a page').'</div>';
	return $output;
}

function theme_tmydoc_inline_folder_hints($term)
{
	$header = array(t("Code"));
	$rows = array(
		array("[folder:".$term->tid."]"),		
	);
	
	//$caption="Inline document link code";
	$output = theme("table",$header,$rows,$atts=NULL,$caption);
	$output .= '<div>'.t('Use this code to insert this folder in to a page').'</div>';
	return $output;
}


function tmydoc_inline_token_values($type, $object = NULL, $options = array()) {
  if ($type == 'node') {
    $node = $object;
    if( isset($node->path) )
    {
    	$tokens['clean-path'] = $node->path;
    }
    else
    {
    	$tokens['clean-path'] = $node->nid;	
    }
    return $tokens;
  }
}

function tmydoc_inline_token_list($type = 'all') {
  if ($type == 'node' || $type == 'all') {
    $tokens['node']['clean-path'] = t("node path name. defaults to nid if none exists");
    return $tokens;
  }
}


/*
Implenetation of hook_filter

Custom filter for including internal "Document" type files inline like [document:nid]

See here for info on how to use this hook
http://api.drupal.org/api/function/hook_filter
http://api.drupal.org/api/file/developer/examples/filter_example.module/6

Also see the "inline" module for how they've used it.

example of a document node
stdClass Object
(
    [nid] => 42
    [type] => document
    [language] => 
    [uid] => 1
    [status] => 1
    [created] => 1236729157
    [changed] => 1236729293
    [comment] => 0
    [promote] => 1
    [moderate] => 0
    [sticky] => 0
    [tnid] => 0
    [translate] => 0
    [vid] => 42
    [revision_uid] => 1
    [title] => Title of the file
    [body] => <p>Description</p>
    [teaser] => <p>Description</p>
    [log] => 
    [revision_timestamp] => 1236729293
    [format] => 2
    [name] => admin
    [picture] => 
    [data] => a:0:{}
    [field_tmydoc_file] => Array
        (
            [0] => Array
                (
                    [fid] => 28
                    [list] => 1
                    [data] => Array
                        (
                            [fid] => 28
                            [width] => 0
                            [height] => 0
                            [duration] => 
                        )

                    [uid] => 1
                    [filename] => WCPFC Legal Holidays.pdf
                    [filepath] => sites/wcpfc.eightyoptions.com/files/WCPFC Legal Holidays_2.pdf
                    [filemime] => application/pdf
                    [filesize] => 14291
                    [status] => 1
                    [timestamp] => 1236729141
                )

        )

    [last_comment_timestamp] => 1236729157
    [last_comment_name] => 
    [comment_count] => 0
    [taxonomy] => Array
        (
        )

)

*/

function tmydoc_inline_filter($op, $delta = 0, $format = -1, $text = '') {
  switch ($op) {
    case 'list':
      return array(
      	0 => t('Inline Taxonomy Document token parsing e.g. [doc:123]'),
      	1 => t('Inline Taxonomy Document token parsing for lists by folder e.g. [folder:123]'),
      );
    case 'description':
      switch($delta)
      {
      	case 0:
	      	return t('[doc:nid] inserts files attached to a Taxonomy Document type node');
      	break;
      	case 1:
      		return t('[folder:tid] inserts the contents of the folder with ID tid as an inline table');
      	break;
      }
    case 'prepare':
      return $text;
    case "process":
  	  return _insert_doc_process_tags($text);
	case 'no cache':
      return TRUE;
  }
}

function tmydoc_inline_filter_tips($delta, $format, $long = FALSE) 
{
	switch($delta)
	{
		case 0:
			return t("Add an inline Document with [doc:nid], where nid is the document ID");
		break;
		case 1:
			return t("Add an inline Document list by Folder with [folder:tid], where tid is the Folder ID");
		break;
	}
}

//also returns a list of fids is an array of file id's which have been found during replacement
function _insert_doc_process_tags($text,&$files=NULL)
{
        //$tag = '/\[(doc|document|doclist):(\d+?)(=.+?)?\]/i';
        $tag = '/\[(doc|document|folder|docicon):(\w+)\]/i'; //matches any "word" so can now be letters OR numbers in the 2nd parameter. \i means case-insensitive
        preg_match_all($tag, $text, $matches, PREG_SET_ORDER);

        $files = array();
        
        foreach($matches as $match)
        {
                $original = $match[0]; //e.g. [document:34];
                $type = $match[1]; //e.g. document
                $nid = $match[2]; //e.g. 34
                
				//get a all the attached files belonging to the matched node (for return purposes)
				//TODO insert this code for indidual document links in the body
				/*$node = node_load($nid,$revision=NULL,$reset=TRUE);
				print_r($node);
				$fids[] = filefield_get_node_files($node);
				*/
				
                if( !empty($match) )
                {
                        if($type != "folder")
                        {
		                    $node = node_load($nid);
		            
			                //if user doesn't have access to view this document node, then return a message
			                if(!node_access('view',$node))
			                {
			                	$processed_tag = '['.t('Document Not Found').']';
			                	$type = NULL;
			                }
		                }

                        switch($type)
                        {
                                case 'doc':
                                case 'document':                                        
                                        $processed_tag = theme('tmydoc_nodefiles',$node,$node->title);
                                        break;
                                case 'docicon':
                                        $processed_tag = theme('tmydoc_nodefiles',$node,$title=FALSE);
                                        break;
                                case 'folder':
                                        
                                        $folder = views_embed_view('tmydocs_folder','default',$nid);                        
                                        
                                        
                                        
                                        $processed_tag = $folder;
                                        
                                        if(!$folder)
                                        {
                                        //	$processed_tag = '<table class="views-table"><thead><tr><th class="views-field">No results for [folder:'.$nid.']</th></tr></thead></table>';	
                                        }
                                        else
                                        {
                                        //	$processed_tag = $folder;
                                        }                                        
                                        
                                        break;
                        }
                        
                        $text = str_replace($original,$processed_tag,$text);                        
                }
        }
        
        return $text;
}

function tmydoc_inline_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL)
{	
	if($node->type =='tmydoc' && $op =="view")
	{
		if(tmydoc_filter_access($delta=0))
		{
			//drupal_set_message('<pre>show tips</pre>');
			//drupal_set_message('<pre>'.print_r($node,true).'</pre>');
			$node->content['body']['#weight'] = 100;
			$node->content['body']['#value'] = theme("tmydoc_inline_doc_hints",$node);
		}	
	}
}

//check if a user has access to this filter
//used to show help text on node pages
function tmydoc_filter_access($delta)
{
	static $access = NULL;

	if(!is_null($access))
	{
		return $access;
	}
	
	global $user;
	
	//don't show tips to anon user
	if($user->uid == 0)
	{
		$access = FALSE;
		return $access;
	}
	
	$access = FALSE;	
	$result = db_query("SELECT format FROM {filters} WHERE module = '%s' AND delta = %d",'tmydoc_inline',$delta);	
	while( $row = db_fetch_object($result) )
	{
		if(filter_access($row->format))
		{
			$access = TRUE;
		}
	}
	
	return $access;
}
