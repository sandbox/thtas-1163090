<?php

function _tmydoc_get_folder_vid() {
  $vocid = variable_get('tmydoc_folder_vid', '');
  if (empty($vocid)) {
    // Check to see if document vocabulary exists
    $vocid = db_result(db_query("SELECT vid FROM {vocabulary} WHERE module='tmydoc'"));
    if ($vocid) {
      // We found a vocabulary, so make sure it is associated with our content.
      $vocabulary = (array) taxonomy_vocabulary_load($vocid);
      $vocabulary['nodes'] = array('tmydoc' => 1);
      $status = taxonomy_save_vocabulary($vocabulary);
    }
    else {
      // Didn't find one, so create vocabulary from scratch.
      $vocabulary = array(
          'name' => t('Folder'),
          'machine_name' => 'tmydoc_folder', 
          'multiple' => 0,
          'required' => 1,
          'hierarchy' => 2,
          'relations' => 0,
          'module' => 'tmydoc',
          'nodes' => array(
              'tmydoc' => 1));
      $status = taxonomy_save_vocabulary($vocabulary);
      $vocid = $vocabulary['vid'];
    }
    
    variable_set('tmydoc_folder_vid', $vocid);
  }
  return $vocid;
}

function _tmydoc_check_node_grants($node,$perm)
{
	global $user;
	if($user->uid == 1)
	{
		return true;
	}
	
	if($user->uid == $node->uid)
	{
		return true;
	}
	
	if(user_access("edit any ".$node->type." content") && $perm == "update")
	{
		return true;
	}
	
	//if nodeaccess is installed, check expicit grants
	if(module_exists('nodeaccess'))
	{
		$grants = _nodeaccess_get_grants($node);
	
		//check roles
		foreach($grants['rid'] as $grant)
		{
			//if the user has this role
			if(in_array($grant['name'],array_values($user->roles)))
			{
				//AND this role has this permission
				if($grant["grant_".$perm])
				{			
					return true;	
				}			
			}
		}
		
		//check explicit user
		return ($grants['uid'][$user->uid]['grant_'.$perm] == 1);
	}
	
	return false;
}

function _tmydoc_node_edit_link($node)
{
	if( _tmydoc_check_node_grants($node,"update") )
	{
			$edit_link_opts = array();
			if(isset($_GET['q']))
			{
				$edit_link_opts = array('query'=>array('destination'=>$_GET['q']),'html'=>TRUE);
			}
			$edit_link = ' <span class="tmydoc-edit-link">['.l(t('edit'),'node/'.$node->nid.'/edit',$edit_link_opts).']</span>';
	}
	
	return $edit_link;
}
